const { Router } = require('express');
const MessageService = require('../services/messageService');

const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    res.data = MessageService.getAll();
    next();
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    const id = req.params.id;
    const data = MessageService.getOne({ id });

    if (data) {
      res.data = data;
    } else {
      res.notFound = true;
    }
    next();
  },
  responseMiddleware
);

router.post(
  '/',
  (req, res, next) => {
    try {
      const data = req.body;

      res.data = data;

      if (!res.err && data) {
        MessageService.create(data);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  (req, res, next) => {
    try {
      if (!res.err) {
        const id = req.params.id;
        const data = req.body;

        const isFound = MessageService.getOne({ id });

        if (isFound) {
          res.data = MessageService.update(id, data);
        } else {
          res.notFound = true;
        }
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    const id = req.params.id;
    const isFound = MessageService.getOne({ id });

    if (isFound) {
      res.data = MessageService.delete(id);
    } else {
      res.notFound = true;
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
