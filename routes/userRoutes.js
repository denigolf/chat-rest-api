const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    res.data = UserService.getAll();
    next();
  },
  responseMiddleware
);

router.get(
  '/:login',
  (req, res, next) => {
    const login = req.params.login;
    const data = UserService.getOne({ login });

    if (data) {
      res.data = data;
    } else {
      res.notFound = true;
    }
    next();
  },
  responseMiddleware
);

router.post(
  '/',
  createUserValid,
  (req, res, next) => {
    try {
      const data = req.body;

      res.data = data;

      if (!res.err && data) {
        UserService.create(data);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateUserValid,
  (req, res, next) => {
    try {
      if (!res.err) {
        const id = req.params.id;
        const data = req.body;

        const isFound = UserService.getOne({ id });

        if (isFound) {
          res.data = UserService.update(id, data);
        } else {
          res.notFound = true;
        }
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    const id = req.params.id;
    const isFound = UserService.getOne({ id });

    if (isFound) {
      res.data = UserService.delete(id);
    } else {
      res.notFound = true;
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
