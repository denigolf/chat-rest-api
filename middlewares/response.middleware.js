const responseMiddleware = (req, res, next) => {
  if (res.notFound) {
    res.status(404).send({
      error: true,
      message: '404. Not found',
    });
    return;
  }

  if (res.err) {
    res.status(400).send({
      error: true,
      message: res.err.message,
    });
  } else {
    res.status(200).send(res.data);
  }

  return;
};

exports.responseMiddleware = responseMiddleware;
