const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
  const newUser = req.body;
  const newUserFields = Object.keys(newUser);

  if (!newUserFields.every((item) => Object.keys(user).includes(item))) {
    res.err = new Error('Redundant fields are not allowed!');

    next();
  }

  if (!newUser.password || newUser.password.length < 3) {
    res.err = new Error('Password must be at least 3 characters!');

    next();
  }

  next();
};

const updateUserValid = (req, res, next) => {
  const newUser = req.body;
  const newUserFields = Object.keys(newUser);

  if (newUserFields.length === 0) {
    res.err = new Error('Empty object is not allowed!');

    next();
  }

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
